#!/usr/bin/env python
# -*- encoding: utf8 -*-
#
# Author: Jerry Shi
# Mail: jerryshi0110@gmail.com
# Created Time: 2016-04-18 20:53:41
# Python version 2.7.5
# ------------------------------------

from django.http import HttpResponse, Http404
from django.template import Context
from django.template.loader import get_template
from django.shortcuts import render_to_response
import datetime

def hello(request):
    return HttpResponse("\t 悯此忘真契，\
                        \n\t生名无处求。\
                        \n\t诗书来不及，\
                        \n\t江海渺难分。")

"""
def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)
"""
def current_datetime(request):
    now = datetime.datetime.now()
    #t = get_template('current_datetime.html')
    #html = t.render(Context({'current_date': now}))
    #return HttpResponse(html)
    return render_to_response('dateapp/current_datetime.html', {'current_date': now})

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    html = "<html><body>In %s hour(s), it will be %s.</body></html>" %(offset, dt)
    return HttpResponse(html)
